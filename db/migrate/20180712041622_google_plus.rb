class GooglePlus < ActiveRecord::Migration[5.2]
  def change
    add_column :sessions, :display_name, :string
    add_column :sessions, :profile_url, :text
    add_column :sessions, :user_id, :string
  end
end
