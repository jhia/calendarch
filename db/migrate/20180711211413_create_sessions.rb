class CreateSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :sessions do |t|
      t.text :access_token
      t.timestamp :expires_in
      t.string :token_type

      t.timestamps
    end
  end
end
