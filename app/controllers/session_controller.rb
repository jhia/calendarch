class SessionController < ApplicationController
  def redirect
    client = Signet::OAuth2::Client.new(client_options)
    redirect_to client.authorization_uri.to_s
  end

  def callback
    client = Signet::OAuth2::Client.new(client_options)
    client.code = params[:code]
    response = client.fetch_access_token!
    upsert_user(client, response)
    session[:authorization] = response
    headers['Authorization'] = response['token_type'] + ' ' +
      response['access_token']
    redirect_to index_url
  end

  def upsert_user(client, response_token)
    service = Google::Apis::PlusV1::PlusService.new
    service.authorization = client
    puts response_token
    result = service.get_person('me')
    user = Session.find_or_initialize_by(user_id: result.id)
    user.profile_url = result.url
    user.display_name = result.display_name
    user.access_token = response_token["access_token"]
    user.expires_in = Time.now.getutc + response_token["expires_in"]
    user.token_type = response_token["token_type"]
    user.save()
  end

end
