class ApplicationController < ActionController::Base
  include GoogleOptions
  include Response
  include ExceptionHandler

  protect_from_forgery unless: -> { request.format.json? }
end
