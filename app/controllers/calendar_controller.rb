require "active_support"

class CalendarController < ApplicationController

  def index
    if session[:authorization].nil?
      redirect_to redirect_url
    end
    @calendar_list = calendars

    rescue ArgumentError
      :resend_auth
  end

  def calendars
    client = Signet::OAuth2::Client.new(client_options)
    client.update!(session[:authorization])

    service = Google::Apis::CalendarV3::CalendarService.new
    service.authorization = client

    service.list_calendar_lists
  end

  def events
    client = Signet::OAuth2::Client.new(client_options)
    client.update!(session[:authorization])

    service = Google::Apis::CalendarV3::CalendarService.new
    service.authorization = client

    event_list = service.list_events(params[:calendar_id],
      max_results: 5,
      single_events: true,
      order_by: 'startTime',
      time_min: Date.today.at_beginning_of_month.to_datetime.iso8601
    )
    json_response(event_list)
    rescue Google::Apis::AuthorizationError
      response = client.refresh!
      session[:authorization] = session[:authorization].merge(response)
      upsert_user(client, reponse)
    retry
    rescue ArgumentError
      response = client.refresh!
      session[:authorization] = session[:authorization].merge(response)
      upsert_user(client, reponse)
    retry
  end

  def new_event
    client = Signet::OAuth2::Client.new(client_options)
    client.update!(session[:authorization])

    service = Google::Apis::CalendarV3::CalendarService.new
    service.authorization = client

    event = Google::Apis::CalendarV3::Event.new({
      start: Google::Apis::CalendarV3::EventDateTime.new(date: params[:start_date], time_zone: params[:start_timezone]),
      end: Google::Apis::CalendarV3::EventDateTime.new(date: params[:end_date], time_zone: params[:end_timezone]),
      summary: params[:summary],
      description: params[:description]
    })

    res = service.insert_event(params[:calendar_id], event)
    json_response(res)
    rescue Google::Apis::AuthorizationError
      response = client.refresh!
      session[:authorization] = session[:authorization].merge(response)
      upsert_user(client, reponse)
    retry
  end

  private

  def resend_auth
    redirect_to redirect_url
  end
end
