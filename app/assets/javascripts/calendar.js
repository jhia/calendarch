// https://tc39.github.io/ecma262/#sec-array.prototype.findIndex
if (!Array.prototype.findIndex) {
  Object.defineProperty(Array.prototype, 'findIndex', {
    value: function(predicate) {
     // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return k.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return k;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return -1.
      return -1;
    },
    configurable: true,
    writable: true
  });
}
window.onload = function () {
  function createEvent(e) {
    var d = e.start.date || e.start.dateTime || e.end.date || e.end.dateTime;
    return {
      'Date': buildDate(d),
      'Title': e.summary,
      'Link': e.htmlLink
    }
  }

  function buildDate(str) {
    var d = str.replace(/T\S+/, '').split('-')
      .map(function (n, i) { return i === 1 ? parseInt(n) - 1 : parseInt(n); });
    return new Date(d[0], d[1], d[2]);
  }

  var calendars = Array.prototype.slice.call(document.getElementsByClassName('calendar-graphic'), 0);
  var events = [];
  var responses = calendars.map(function (c) { return fetch('/events/' + encodeURIComponent(c.dataset.calendarId), {
    method: 'GET',
    credentials: 'same-origin'
  }).then(function (res) { return res.json(); }); });
  Promise.all(responses)
  .then(function (results) {
    results.forEach(function (res, i) {
      events[i] = res.items.map(createEvent);
      caleandar(calendars[i], events[i]);
    });
  })
  .catch(function (e) { alert(e.message); });

  Array.prototype.slice.call(document.querySelectorAll('a[data-target-calendar]'), 0)
  .forEach(function (a) {
    a.onclick = function () {
      var content = this.parentElement.parentElement.getElementsByClassName('card-content')[0];
      content.dataset.visible = content.dataset.visible === 'true' ? 'false' : 'true';
    };

    var form = document.querySelector('form[data-target-calendar="' + a.dataset.targetCalendar + '"]');
    form.addEventListener('submit', function (e) {
      e.preventDefault();

      if (this.summary.value.length < 1) return ;
      if (!/^\d{4,4}\-\d{1,2}-\d{1,2}$/.test(this.start_date.value)) return ;
      if (this.end_date.value.length > 0 &&
        !/^\d{4,4}\-\d{1,2}-\d{1,2}$/.test(this.end_date.value)) return ;

      var newEvent = {
        summary: this.summary.value,
        description: this.description.value,
        'start_date': this.start_date.value,
        'start_timezone': this.dataset.calendarTimezone,
        'end_date': this.end_date.value.length > 0 ? this.end_date.value : this.start_date.value,
        'end_timezone': this.dataset.calendarTimezone
      };

      fetch('/events/' + encodeURIComponent(form.dataset.targetCalendar), {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        credentials: 'same-origin',
        body: JSON.stringify(newEvent)
      })
      .then(function (res) { return res.json(); })
      .then(function (res) {
        var index = calendars.findIndex(function (c) {
          return c.dataset.calendarId === form.dataset.targetCalendar;
        });
        events[index].push(createEvent(res));
        calendars[index].removeChild(calendars[index].firstChild);
        caleandar(calendars[index], events[index]);
        form.reset();
      })
      .catch(function (e) { alert(e.message) });
    });
  });

  var datePickers = document.querySelectorAll('.datepicker');
  var instances = M.Datepicker.init(datePickers, {
    defaultDate: new Date(),
    minDate: new Date(),
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
};
