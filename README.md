# CALENDARCH (Calendar Challenge)

Connects with Google in order to show all your calendars with events, and insert
events to calendars.

Based on this [ReadySteadyCode
post](https://readysteadycode.com/howto-integrate-google-calendar-with-rails).

## Features

This rails app uses `google-api-client` gem to manage Google APIs, and `signet`
for Google APIs authorization client.

Also, saves session into PostgreSQL database, to configure, change
`config/database.yml` or define var `DATABASE_URL` in your environment.

## TO-DO

- Validation in Model
- Puma integration
- Clean unused files
