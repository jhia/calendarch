Rails.application.routes.draw do
  root 'calendar#index', as: 'index'
  get '/redirect', to: 'session#redirect', as: 'redirect'
  get '/callback', to: 'session#callback', as: 'callback'
  get '/events/:calendar_id', to: 'calendar#events', as: 'events', calendar_id: /[^\/]+/, format: false
  post '/events/:calendar_id', to: 'calendar#new_event', as: 'new_event', calendar_id: /[^\/]+/, format: false
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
